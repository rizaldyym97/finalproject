import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class News extends StatefulWidget {
  const News({ Key key }) : super(key: key);

  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  List<Jumlah> listjumlah = [];
  Repository repository = Repository();

  getData() async{
    listjumlah = await repository.getData();
    setState(() {
      
    });
  }

  @override
  void initState() {
    getData();
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 40),
              child: Text(
                "Data Kasus Corona",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.w500,
                    fontSize: 25),
              ),
            ),
            SizedBox(height: 30,),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Positif",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                (listjumlah?.length > 0 ? listjumlah[0].positif: 'menghitung...'),
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.w300,
                    fontSize: 50),
              ),
            ),
            SizedBox(height: 30,),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Sembuh",
                style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                (listjumlah?.length > 0 ? listjumlah[0].sembuh: 'menghitung...'),
                style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.w300,
                    fontSize: 50),
              ),
            ),
            SizedBox(height: 30,),
            
            
            
            
          ],
        ),
      )
      // body: ListView.separated(
      //   itemBuilder: (context, index){
      //    return Container(
      //     child: Text(listjumlah[index].positif),
      //   );
      // }, 
      // separatorBuilder: (context, index){
      //   return Divider();
      // }, itemCount: listjumlah.length)
      
    );
  }
}

class Jumlah {
  String nama;
  String positif;
  String sembuh;
  String meninggal;

  Jumlah({this.nama, this.positif, this.sembuh, this.meninggal});

  factory Jumlah.jumlahSemua(Map<String, dynamic> object)
  {
    return Jumlah(
      nama: object['name'],
      positif: object['positif'],
      sembuh: object['sembuh'],
      meninggal: object['meniggal']
    );
  }
}
  // static Future<Jumlah> connectToAPI(String nama) async{
  //   String apiURL = "https://api.kawalcorona.com/" + nama;

  //   var apiResult = await http.get(Uri.parse(apiURL));
  //   var jsonObject = json.decode(apiResult.body);
  //   List<Jumlah> jumlah = jsonObject.map
    
  // }
class Repository {
  final _baseUrl = "https://api.kawalcorona.com/indonesia/";

  Future getData() async {
    try {
      final response = await http.get(Uri.parse(_baseUrl));

      if (response.statusCode == 200){
        print(response.body);
        Iterable it = jsonDecode(response.body);
        List<Jumlah> jumlahh = it.map((e) => Jumlah.jumlahSemua(e)).toList();
        return jumlahh;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}