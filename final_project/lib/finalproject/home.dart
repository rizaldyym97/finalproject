import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({ Key key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: Container(
        padding: EdgeInsets.all(30),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            MyHome(title: "Teknologi", icon: Icons.ad_units, warna: Colors.blue,),
            MyHome(title: "Bisnis", icon: Icons.assessment_outlined, warna: Colors.green,),
            MyHome(title: "Liburan", icon: Icons.beach_access_outlined, warna: Colors.purple,),
            MyHome(title: "Kesehatan", icon: Icons.health_and_safety_outlined, warna: Colors.red,),
            MyHome(title: "Olahraga", icon: Icons.sports_basketball_outlined, warna: Colors.orange,),
            MyHome(title: "Politik", icon: Icons.account_balance, warna: Colors.amber,)
            
          ],),
      ),
    );
  }
}
class MyHome extends StatelessWidget {

  MyHome({this.title, this.icon, this.warna});

  final String title;
  final IconData icon;
  final MaterialColor warna;

  @override
  Widget build(BuildContext context) {
    return  Card(
              margin: EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {},
                splashColor: Colors.blueAccent,
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        icon,
                        size: 70.0,
                        color: warna,
                      ),
                      Text(title, style: TextStyle(fontSize: 17),),
                    ],),
                ),
              ),
            );
  }
}