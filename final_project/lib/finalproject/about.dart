import 'package:final_project/main.dart';
import 'package:flutter/material.dart';

class About extends StatefulWidget {
  const About({ Key key }) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      
       body: Column(
         crossAxisAlignment: CrossAxisAlignment.center,
         children: <Widget>[
           Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                "About Me",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.w500,
                    fontSize: 30),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.network(
                "https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1887&q=80",
                height: 100,
                width: 100,
              ),
            ),
            SizedBox(height: 10,),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Rizaldy Yahya",
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
            ),
                  SizedBox(height: 10),
            Align(
              alignment: Alignment.center,
              child: Text(
                "rizaldyym97@gmail.com",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 20),
              ),
            ),
            SizedBox(height: 10),
            Align(
              alignment: Alignment.center,
              child: Text(
                "08123456789",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 20),
              ),
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  child: Image.network(
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Facebook_f_logo_%282019%29.svg/900px-Facebook_f_logo_%282019%29.svg.png",
                    height: 30,
                    width: 30,
                  )
                ),
                Container(
                  child: Image.network(
                    "https://cdn.icon-icons.com/icons2/836/PNG/512/Twitter_icon-icons.com_66803.png",
                    height: 30,
                    width: 30,
                  )
                ),
                Container(
                  child: Image.network(
                    "https://upload.wikimedia.org/wikipedia/commons/a/a5/Instagram_icon.png",
                    height: 30,
                    width: 30,
                  )
                ),
              ],
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 20),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
              ),
            )
            ],
       )
      
      //       SizedBox(height: 30),
      
      //       SizedBox(height: 10),
      //       Align(
      //         alignment: Alignment.center,
      //         child: Text(
      //           "rizaldyym97@gmail.com",
      //           style: TextStyle(
      //               fontWeight: FontWeight.w300,
      //               fontSize: 20),
      //         ),
      //       ),
      //       SizedBox(height: 10),
      //       Align(
      //         alignment: Alignment.center,
      //         child: Text(
      //           "08123456789",
      //           style: TextStyle(
      //               fontWeight: FontWeight.w300,
      //               fontSize: 20),
      //         ),
              
      //       ),
      //     ],
      //   ),
      // ),
      
    );
  }
}