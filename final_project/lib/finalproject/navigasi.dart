import 'package:final_project/finalproject/about.dart';
import 'package:final_project/finalproject/login.dart';
import 'package:final_project/finalproject/home.dart';
import 'package:final_project/finalproject/news.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class Navigasi extends StatefulWidget {
  
  const Navigasi({ Key key }) : super(key: key);

  @override
  _NavigasiState createState() => _NavigasiState();
}

class _NavigasiState extends State<Navigasi> {
  int currentIndexx = 0;
  final screens = [
  Home(),
  News(),
  About()
];
  Future<void> _signOut() async{
    await FirebaseAuth.instance.signOut();
  }
  

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if(auth.currentUser != null){
      print(auth.currentUser.email);
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      
      appBar: AppBar(title: Text("Aplikasiku"),
      ),
      
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Selamat Datang"),
              accountEmail: Text("${auth.currentUser.email}"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage("https://cdn.icon-icons.com/icons2/2506/PNG/512/user_icon_150670.png"),
              ),
              ),
              
            ListTile(
              leading: Icon(Icons.logout),
              title: Text("Logout"),
              onTap: () {
               _signOut().then((value) => Get.off(LoginScreen()));
              },
            ),
          ],
        ),
      ),

      body: 
      screens[currentIndexx],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndexx,
        onTap: (index) => setState(() => currentIndexx = index),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home"
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.document_scanner),
            label: "News",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "About Me",
          )
        ],
      ), 
    );
  }
}
